container_repo = ayoung/jupyterhub-traefik
container_version = v0.1

docker-build:
	docker build -t ${container_repo}:${container_version} .

docker-tag:
	docker tag ${container_repo}:${container_version} ${container_repo}:latest

docker run:
	docker run -it --rm ${container_repo}:${container_version}