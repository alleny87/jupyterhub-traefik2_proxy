jupyterhub>=4.0.0
jupyterlab>=3.6
notebook>=6.5
jupyterhub-traefik-proxy>=1.0.0b3