#!/bin/bash

pip3 install --user -U pip setuptools

pip3 install --user -r /workspaces/traefik-proxy/requirements.txt

python3 -m pip install jupyterhub-traefik-proxy -U --pre