FROM python:3.10

RUN mkdir -p /opt/jupyter/

ADD ./requirements.txt /build/
ADD ./entrypoint.sh /entrypoint.sh

RUN python3.10 -m pip install -U pip setuptools setuptools-rust && \
    python3.10 -m pip install -U -r /build/requirements.txt

VOLUME /opt/jupyter
WORKDIR /opt/jupyter

EXPOSE 8000

ENTRYPOINT /entrypoint.sh


